## Simple Information visualization Project

This is a simple d3 visualization of 4 Anscombe datasets used for an information visualization class. 
The following are included:

* messing with css options
* interacting via dropdown 
* interacting via mouseover
* interacting via mouse clicks (part 3, tooltip appears)


## Getting Started

Run a server from your local computer. 
``
python3 -m http.server
``

## Built with
* [d3](https://d3js.org/)


